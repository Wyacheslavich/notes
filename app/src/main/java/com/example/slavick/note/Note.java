package com.example.slavick.note;

public class Note {
    private String title;
    private String text;

    public String getTitle() {
        return title;
    }

    public String getText() {
        return text;
    }

    public Note(String title, String text) {
        this.title = title;
        this.text = text;
    }
}

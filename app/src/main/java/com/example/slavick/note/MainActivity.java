package com.example.slavick.note;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;


public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_land);
        getSupportFragmentManager().beginTransaction().replace(R.id.list_item, ListFragment.getInstance()).commit();
        getSupportFragmentManager().beginTransaction().replace(R.id.read_note, ReadFragment.getInstance()).commit();

    }
}

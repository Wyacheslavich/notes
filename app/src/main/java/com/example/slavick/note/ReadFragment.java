package com.example.slavick.note;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class ReadFragment extends Fragment {

    private static ReadFragment instance;

    public static ReadFragment getInstance() {
        if (instance == null){
            instance = new ReadFragment();
        }
        return instance;
    }

    TextView title;
    TextView text;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.notes_read, container, false);
        title = root.findViewById(R.id.title);
        text = root.findViewById(R.id.text);
        return root;
    }
}

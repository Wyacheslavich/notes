package com.example.slavick.note;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class ListFragment extends Fragment implements MyRecyclerAdapter.OnRecyclerItemClick {

    private static ListFragment instance;
    List<Note> notes = new ArrayList<>();
    MyRecyclerAdapter adapter;
    Context context;

    public static ListFragment getInstance() {
        if (instance == null) {
            instance = new ListFragment();
        }
        return instance;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    TextView text;
    TextView title;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.notes_list, container, false);
        Note note = new Note("My first Title", "My first note text, thank you for attention");
        final RecyclerView list = root.findViewById(R.id.list_item);
        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        list.setLayoutManager(layoutManager);
        notes.add(note);
        adapter = new MyRecyclerAdapter(context, notes, this);
        list.setAdapter(adapter);
        text = root.findViewById(R.id.text);
        title = root.findViewById(R.id.title);
        return root;
    }


    @Override
    public void onClick(int position) {

    }
}
